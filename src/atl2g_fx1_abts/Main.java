package atl2g_fx1_abts;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import static javafx.application.Application.launch;
import javafx.collections.FXCollections;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Pane;

public class Main
        extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Calcul du BMR");

        Pane root = new Pane();

        Label drawDonnees = new Label("Données");
        drawDonnees.setUnderline(true);
        drawDonnees.setLayoutX(20);
        drawDonnees.setLayoutY(20);
        root.getChildren().add(drawDonnees);

        Label drawTaille = new Label("Taille (cm)");
        drawTaille.setLayoutX(20);
        drawTaille.setLayoutY(40);
        root.getChildren().add(drawTaille);

        TextField fieldTaille = new TextField();
        fieldTaille.setPrefColumnCount(15);
        fieldTaille.setLayoutX(120);
        fieldTaille.setLayoutY(35);
        root.getChildren().add(fieldTaille);

        Label drawPoids = new Label("Poids (kg)");
        drawPoids.setLayoutX(20);
        drawPoids.setLayoutY(60);
        root.getChildren().add(drawPoids);

        TextField fieldPoids = new TextField();
        fieldPoids.setPrefColumnCount(15);
        fieldPoids.setLayoutX(120);
        fieldPoids.setLayoutY(55);
        root.getChildren().add(fieldPoids);

        Label drawAge = new Label("Age (années)");
        drawAge.setLayoutX(20);
        drawAge.setLayoutY(80);
        root.getChildren().add(drawAge);

        TextField fieldAge = new TextField();
        fieldAge.setPrefColumnCount(15);
        fieldAge.setLayoutX(120);
        fieldAge.setLayoutY(75);
        root.getChildren().add(fieldAge);

        Label drawSexe = new Label("Sexe");
        drawSexe.setLayoutX(20);
        drawSexe.setLayoutY(100);
        root.getChildren().add(drawSexe);

        final ToggleGroup group = new ToggleGroup();

        RadioButton sexeM = new RadioButton("Homme");
        sexeM.setLayoutX(120);
        sexeM.setLayoutY(100);
        sexeM.setToggleGroup(group);
        sexeM.setUserData("M");
        root.getChildren().add(sexeM);

        RadioButton sexeF = new RadioButton("Femme");
        sexeF.setLayoutX(220);
        sexeF.setLayoutY(100);
        sexeF.setToggleGroup(group);
        sexeF.setUserData("F");
        root.getChildren().add(sexeF);

        Label drawStyle = new Label("Style de vie");
        drawStyle.setLayoutX(20);
        drawStyle.setLayoutY(120);
        root.getChildren().add(drawStyle);

        ChoiceBox styleCB = new ChoiceBox(FXCollections.observableArrayList(
                "Sédentaire", "Peu actif", "Actif", "Fort actif", "Extremement actif")
        );
        styleCB.setLayoutX(120);
        styleCB.setLayoutY(120);
        root.getChildren().add(styleCB);

        Label drawResultat = new Label("Résultats");
        drawResultat.setUnderline(true);
        drawResultat.setLayoutX(320);
        drawResultat.setLayoutY(20);
        root.getChildren().add(drawResultat);

        Label drawBMR = new Label("BMR");
        drawBMR.setLayoutX(320);
        drawBMR.setLayoutY(40);
        root.getChildren().add(drawBMR);

        TextField fieldBMR = new TextField();
        fieldBMR.setPrefColumnCount(15);
        fieldBMR.setLayoutX(420);
        fieldBMR.setLayoutY(35);
        root.getChildren().add(fieldBMR);

        Label drawCalories = new Label("Calories");
        drawCalories.setLayoutX(320);
        drawCalories.setLayoutY(60);
        root.getChildren().add(drawCalories);

        TextField fieldCalories = new TextField();
        fieldCalories.setPrefColumnCount(15);
        fieldCalories.setLayoutX(420);
        fieldCalories.setLayoutY(55);
        root.getChildren().add(fieldCalories);

        Button button = new Button("Calcul du BMR");
        button.setLayoutX(20);
        button.setLayoutY(150);
        root.getChildren().add(button);
        button.setOnAction((event) -> {
            if (fieldTaille.getText() != null
                    && fieldPoids.getText() != null
                    && fieldAge.getText() != null
                    && group.getSelectedToggle() != null
                    && styleCB.getValue().toString() != null) {

                int taille = Integer.parseInt(fieldTaille.getText());
                int poids = Integer.parseInt(fieldPoids.getText());
                int age = Integer.parseInt(fieldAge.getText());
                char sexe = group.getSelectedToggle().getUserData().toString().charAt(0);
                String strStyle = styleCB.getValue().toString();
                double styleModifier = (strStyle.equals("Sédentaire")) ? 1.2 : ((strStyle.equals("Peu actif")) ? 1.375 : ((strStyle.equals("Actif")) ? 1.55 : ((strStyle.equals("Fort actif")) ? 1.725
                        : /*Extremement actif */ 1.9)));
                double BMR = (sexe == 'M') ? 13.7 * poids + 5 * taille + 6.8 * age + 66
                        : 9.6 * poids + 1.8 * taille + 4.7 * age + 655;
                double calories = BMR * styleModifier;
                fieldBMR.setText(String.valueOf(BMR));
                fieldBMR.setStyle("-fx-text-fill: green;");
                fieldCalories.setText(String.valueOf(calories));
                fieldCalories.setStyle("-fx-text-fill: green;");
            } else {
                fieldBMR.setText("REMPLISSEZ TOUT");
                fieldBMR.setStyle("-fx-text-fill: red;");
                fieldCalories.setText("REMPLISSEZ TOUT");
                fieldCalories.setStyle("-fx-text-fill: red;");
            }
        });

        Scene scene = new Scene(root, 640, 200);

        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
